﻿using Composite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeAddons.DataUrl
{
    public interface IInternalUrlParser
    {
        String WriteKey(IData Data);
        object ReadKey(String Key);
    }
}
