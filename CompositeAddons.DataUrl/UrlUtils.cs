﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeAddons.DataUrl
{
	public static class UrlUtils
	{
        internal class UrlMatch
        {
            public int Index;
            public string Value;
        }

        /// <summary>
        /// Same implementation as Composite.Core.WebClient.UrlUtils
        /// 
        /// Finds all the urls that start with <paramref name="urlPrefix"/>.
        /// We assume that each url ends before one of the following strings:
        /// double quote, single quote, or &#39; which is single quote mark (') encoded in xml attribute 
        /// </summary>
        /// <param name="html">The html content.</param>
        /// <param name="urlPrefix">The url prefix</param>
        /// <returns>List of urls, sorted by the order they appear</returns>
        /// 
        /// <summary>
        /// Finds all the urls that start with <paramref name="urlPrefix"/>.
        /// We assume that each url ends before one of the following strings:
        /// double quote, single quote, or &#39; which is single quote mark (') encoded in xml attribute 
        /// </summary>
        /// <param name="html">The html content.</param>
        /// <param name="urlPrefix">The url prefix</param>
        /// <returns>List of urls, sorted by the order they appear</returns>
        internal static List<UrlMatch> FindUrlsInHtml(string html, string urlPrefix)
        {
            var result = new List<UrlMatch>();

            int startIndex = 0;

            while (true)
            {
                int urlOffset = html.IndexOf(urlPrefix, startIndex, StringComparison.OrdinalIgnoreCase);
                if (urlOffset < 5) break;

                int prefixEndOffset = urlOffset + urlPrefix.Length;
                int endOffset = -1;

                char lastQuoteSymbol = html[urlOffset - 1];

                // If starts with a quote symbol- should end with the same quote symbol
                if (lastQuoteSymbol == '\''
                    || lastQuoteSymbol == '\"')
                {
                    endOffset = html.IndexOf(lastQuoteSymbol, prefixEndOffset);
                }
                else if (lastQuoteSymbol == ';' && urlOffset > 5)
                {
                    string fiveCharsPrefix = html.Substring(urlOffset - 5, 5);

                    if (fiveCharsPrefix == "&#34;"
                        || fiveCharsPrefix == "&#39;")
                    {
                        endOffset = html.IndexOf(fiveCharsPrefix, prefixEndOffset, StringComparison.Ordinal);
                    }
                }

                // Skippnig match if the quotes aren't defined
                if (endOffset < 0)
                {
                    startIndex = prefixEndOffset;
                    continue;
                }

                // Skipping html anchors 
                int hashSignIndex = html.IndexOf('#', prefixEndOffset, endOffset - prefixEndOffset);
                if (hashSignIndex > 0)
                {
                    endOffset = hashSignIndex;
                }

                result.Add(new UrlMatch
                {
                    Index = urlOffset,
                    Value = html.Substring(urlOffset, endOffset - urlOffset)
                });

                startIndex = endOffset;
            }

            return result;
        }
        
        /// <summary>
        /// Same implementation as FindUrlsInHtml(string html, string urlPrefix), but expects
        /// a StringBuilder so it can be used by inline search and replace actions
        /// </summary>
        /// <param name="html">The html content.</param>
        /// <param name="urlPrefix">The url prefix</param>
        /// <returns>List of urls, sorted by the order they appear</returns>
        /// <see cref="FindUrlsInHtml"/>
        internal static List<UrlMatch> FindUrlsInHtml(StringBuilder html, string urlPrefix)
        {
            var result = new List<UrlMatch>();

            int startIndex = 0;

            while (true)
            {
                int urlOffset = html.IndexOf(urlPrefix, startIndex);
                if (urlOffset < 5) break;

                int prefixEndOffset = urlOffset + urlPrefix.Length;
                int endOffset = -1;

                char lastQuoteSymbol = html[urlOffset - 1];

                // If starts with a quote symbol- should end with the same quote symbol
                if (lastQuoteSymbol == '\''
                    || lastQuoteSymbol == '\"')
                {
                    endOffset = html.IndexOf(lastQuoteSymbol, prefixEndOffset);
                }
                else if (lastQuoteSymbol == ';' && urlOffset > 5)
                {
                    string fiveCharsPrefix = html.ToString(urlOffset - 5, 5);

                    if (fiveCharsPrefix == "&#34;"
                        || fiveCharsPrefix == "&#39;")
                    {
                        endOffset = html.IndexOf(fiveCharsPrefix, prefixEndOffset);
                    }
                }

                // Skippnig match if the quotes aren't defined
                if (endOffset < 0)
                {
                    startIndex = prefixEndOffset;
                    continue;
                }

                // Skipping html anchors 
                int hashSignIndex = html.ToString(prefixEndOffset, endOffset - prefixEndOffset).IndexOf('#', 0);
                if (hashSignIndex > 0)
                {
                    endOffset = hashSignIndex;
                }

                result.Add(new UrlMatch
                {
                    Index = urlOffset,
                    Value = html.ToString(urlOffset, endOffset - urlOffset)
                });

                startIndex = endOffset;
            }

            return result;
        }


        /* ========= StringBuilder IndexOf helper methods ========= */

        internal static int IndexOf(this StringBuilder builder, char value, int startIndex = 0, bool ignoreCase = true)
        {
            for (int i = startIndex; i < builder.Length; i++)
            {
                if (ignoreCase)
                {
                    if (char.ToUpperInvariant(builder[i]) == char.ToUpperInvariant(value))
                    {
                        return i;
                    }
                }
                else { 
                    if (builder[i] == value)
                    {
                        return i;
                    }
                }
            }

            return -1;
        }

        internal static int IndexOf(this StringBuilder builder, string value, int startIndex = 0, bool ignoreCase = true)
        {
            int vIndex;
            int vLength = value.Length;
            int max = (builder.Length - vLength) + 1;

            switch (ignoreCase)
            {
                case false:
                    {
                        for (int i = startIndex; i < max; i++)
                        {
                            if (builder[i] == value[0])
                            {
                                vIndex = 1;

                                while ((vIndex < vLength) && (builder[i + vIndex] == value[vIndex]))
                                {
                                    vIndex++;
                                }
                                if (vIndex == vLength)
                                {
                                    return i;
                                }
                            }
                        }
                    }
                    break;

                case true:
                    {
                        for (int j = startIndex; j < max; j++)
                        {
                            if (char.ToUpperInvariant(builder[j]) == char.ToUpperInvariant(value[0]))
                            {
                                vIndex = 1;
                                while ((vIndex < vLength) && (char.ToUpperInvariant(builder[j + vIndex]) == char.ToUpperInvariant(value[vIndex])))
                                {
                                    vIndex++;
                                }
                                if (vIndex == vLength)
                                {
                                    return j;
                                }
                            }
                        }
                    }
                    break;
            }

            return -1;
        }
    }
}