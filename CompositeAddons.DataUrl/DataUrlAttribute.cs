﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompositeAddons.DataUrl
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class DataUrlAttribute : Attribute
    {
        public String Name { get; set; }
        public Type Parser { get; set; }
    }
}