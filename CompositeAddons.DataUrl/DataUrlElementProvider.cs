﻿using Composite.C1Console.Elements;
using Composite.C1Console.Elements.Plugins.ElementProvider;
using Composite.C1Console.Security;
using Composite.Core.ResourceSystem;
using Composite.Core.ResourceSystem.Icons;
using Composite.Data;
using Composite.Data.Types;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositeAddons.DataUrl
{
    [ConfigurationElementType(typeof(DataUrlElementProviderData))]
    public class DataUrlElementProvider : IHooklessElementProvider
    {
        private ElementProviderContext _context;

        public DataUrlElementProvider()
        {
        }

        public Composite.C1Console.Elements.ElementProviderContext Context
        {
            set
            {
                _context = value;
            }
        }

        public IEnumerable<Composite.C1Console.Elements.Element> GetRoots(Composite.C1Console.Elements.SearchToken searchToken)
        {
            List<Element> roots = new List<Element>();

            foreach (var pageElement in ElementFacade.GetRoots(new ElementProviderHandle("PageElementProvider"), searchToken))
            {
                var element = new Element(new ElementHandle(_context.ProviderName, pageElement.ElementHandle.EntityToken), pageElement.VisualData);
                foreach (var kv in pageElement.PropertyBag)
                {
                    element.PropertyBag.Add(kv.Key, kv.Value);
                }

                roots.Add(element);
            }


            return roots;
        }

        public IEnumerable<Composite.C1Console.Elements.Element> GetChildren(Composite.C1Console.Security.EntityToken entityToken, Composite.C1Console.Elements.SearchToken searchToken)
        {
            List<Element> children = new List<Element>();

            foreach (var pageElement in ElementFacade.GetChildren(new ElementHandle("PageElementProvider", entityToken), searchToken))
            {
                var elementEntityToken = pageElement.ElementHandle.EntityToken;

                var element = new Element(new ElementHandle(_context.ProviderName, elementEntityToken), pageElement.VisualData);

                foreach (var kv in pageElement.PropertyBag)
                {
                    element.PropertyBag.Add(kv.Key, kv.Value);
                }

                if (!element.PropertyBag.ContainsKey("Uri"))
                {
                    if (elementEntityToken is DataEntityToken)
                    {
                        DataEntityToken dataEntityToken = (DataEntityToken)element.ElementHandle.EntityToken;

                        if (DataUrlManager.GetRegisteredTypes().Contains(dataEntityToken.InterfaceType))
                        {
                            var dataUrl = DataUrlManager.GetInternalUrl(dataEntityToken.Data);
                            if (dataUrl != null)
                            {
                                element.PropertyBag.Add("Uri", dataUrl);
                            }
                        }
                    }
                }

                children.Add(element);
            }

            return children;
        }

        internal sealed class DataUrlElementProviderAssembler : IAssembler<IHooklessElementProvider, HooklessElementProviderData>
        {
            public IHooklessElementProvider Assemble(IBuilderContext context, HooklessElementProviderData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
            {
                return new DataUrlElementProvider();
            }
        }

        [Assembler(typeof(DataUrlElementProviderAssembler))]
        internal sealed class DataUrlElementProviderData : HooklessElementProviderData
        {
        }
    }
}
