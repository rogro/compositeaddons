﻿using Composite.Data;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Composite.Core.Application;
using Composite.Data.Types;
using Composite.Core.Routing;
using System.Text.RegularExpressions;
using System.Text;
using Composite.Core;

namespace CompositeAddons.DataUrl
{
    public static class DataUrlManager
    {
        private static readonly string LogTitle = typeof(DataUrlManager).Name;

        private static List<DataUrlRegistration> _registrations;
        private static Action<UnhandledUrl> _unhandledUrlAction;

        static DataUrlManager()
        {
            // Initialization

            _registrations = new List<DataUrlRegistration>();

            _unhandledUrlAction = (unhandled) =>
            {
                Log.LogWarning(LogTitle, String.Format("Unhandled internal url '{0}'", unhandled.Url));
            };

            // Resolve static methods with DataUrlAttribute

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var attributedMethods = assemblies
                                  .SelectMany(t => t.GetTypes())
                                  .SelectMany(t => t.GetMethods())
                                  .Where(m => m.GetCustomAttribute<DataUrlAttribute>(false) != null);

            foreach (var method in attributedMethods)
            {
                var dataUrlAttribute = (DataUrlAttribute)method.GetCustomAttributes(typeof(DataUrlAttribute)).First();

                if (!method.IsStatic)
                {
                    throw new Exception("DataRoute methods must be static");
                }
                if (method.ReturnType != typeof(String))
                {
                    throw new Exception("DataRoute methods must return String");
                }

                var dataType = (method.GetParameters().Count() == 1) ? method.GetParameters().First().ParameterType : null;

                if (!(typeof(IData).IsAssignableFrom(dataType)))
                {
                    throw new Exception("DataRoute method does not match signature 'String Method(IData data)'");
                }

                String name = dataUrlAttribute.Name ?? dataType.Name.ToLowerInvariant();

                IInternalUrlParser urlParser = null;

                if (dataUrlAttribute.Parser != null)
                {
                    urlParser = (IInternalUrlParser)System.Activator.CreateInstance(dataUrlAttribute.Parser);
                }

                Register(name, dataType, (f) => { return (String)method.Invoke(null, new[] { f }); });

            }
        }

        public static void Register<T>(String Name, Func<T, String> resolveFunc, IInternalUrlParser Parser = null) where T : IData
        {
            var registration = new DataUrlRegistration 
            { 
                Name = Name, 
                Type = typeof(T), 
                Function = resolveFunc,
                Parser = Parser
            };

            _registrations.Add(registration);
            _registrations.Sort(TypeAssignabilityComparer.Instance);
        }

        private static void Register(String Name, Type type, Func<dynamic, String> resolveFunc, IInternalUrlParser Parser = null)
        {
            var registration = new DataUrlRegistration 
            { 
                Name = Name, 
                Type = type, 
                Function = resolveFunc,
                Parser = Parser
            };

            _registrations.Add(registration);
            _registrations.Sort(TypeAssignabilityComparer.Instance);
        }

        private static void Register(String Name, Type type, Delegate resolveFunc, IInternalUrlParser Parser = null)
        {
            var registration = new DataUrlRegistration 
            { 
                Name = Name, 
                Type = type, 
                Function = resolveFunc,
                Parser = Parser
            };

            _registrations.Add(registration);
            _registrations.Sort(TypeAssignabilityComparer.Instance);
        }

        public static Type[] GetRegisteredTypes()
        {
            return _registrations.Select(r => r.Type).ToArray();
        }

        public static String GetUrl<T>(this T Item) where T : IData 
        {
            var dataType = typeof(T);

            var registration = _registrations.FirstOrDefault(r => r.Type.IsAssignableFrom(dataType));
            if (registration != null)
            {
                return ResolveUrl(Item, registration.Function);
            }

            return null;
        }

        public static String GetUrl(Type dataType, object key)
        {
            var registration = _registrations.FirstOrDefault(r => r.Type.IsAssignableFrom(dataType));
            if (registration != null)
            {
                try
                {
                    var data = DataFacade.GetDataByUniqueKey(registration.Type, key);

                    return ResolveUrl(data, registration.Function);
                }
                catch { }
            }

            return null;
        }

        public static String GetUrl(String dataTypeName, object key)
        {
            var registration = _registrations.FirstOrDefault(r => r.Type.FullName.Equals(dataTypeName, StringComparison.InvariantCultureIgnoreCase));
            if (registration != null)
            {
                try
                {
                    var data = DataFacade.GetDataByUniqueKey(registration.Type, key);

                    return ResolveUrl(data, registration.Function);
                }
                catch { }
            }

            return null;
        }

        public static String GetInternalUrl(IData data)
        {
            var registration = _registrations.FirstOrDefault(r => r.Type.IsAssignableFrom(data.GetDataEntityToken().InterfaceType));
            if (registration != null)
            {
                var urlParser = registration.Parser ?? DefaultInternalUrlParser.Instance;

                return String.Format("~/{0}({1})", registration.Name, urlParser.WriteKey(data));
            }

            return null;
        }

        private static String ResolveUrl(Object Item, Delegate Function)
        {
            return (String)Function.DynamicInvoke(Item);
        }

        public static String PageRelativeUrl(this IPage Page, String PathInfo)
        {
            return PageRelativeUrl(Page.Id, PathInfo);
        }

        public static String PageRelativeUrl(Guid PageId, String PathInfo)
        {
            var urlData = PageUrls.ParseUrl(String.Format("~/page({0})/{1}", PageId, PathInfo));
            urlData.PublicationScope = DataScopeManager.CurrentDataScope.ToPublicationScope();

            return PageUrls.BuildUrl(urlData, UrlKind.Public, new UrlSpace());
        }

        public static string ChangeRenderingDataUrlsToPublic(string html)
        {
            StringBuilder parsedHtml = null;

            foreach (var registration in _registrations.OrderByDescending(r => r.Name.Length))
            {
                String typePrefix = String.Concat("/", registration.Name);

                // lookup urls based on type prefix

                List<UrlUtils.UrlMatch> matches = (parsedHtml != null)
                    ? UrlUtils.FindUrlsInHtml(parsedHtml, typePrefix)
                    : UrlUtils.FindUrlsInHtml(html, typePrefix);

                // iterate matches in reverse order so we can do an inline replace

                if (matches.Count > 0)
                {
                    var urlParser = registration.Parser ?? DefaultInternalUrlParser.Instance;

                    for (int i = matches.Count - 1; i >= 0; i--)
                    {
                        var match = matches[i];

                        var internalUrl = match.Value;

                        // check for opening/closing brackets and allow encoded brackets

                        int openOffset, closeStart, closeEnd;

                        if (internalUrl[typePrefix.Length] == '(')
                        {
                            openOffset = typePrefix.Length + 1;
                        }
                        else if (internalUrl.IndexOf("%28", StringComparison.InvariantCulture) == typePrefix.Length)
                        {
                            openOffset = typePrefix.Length + 3;
                        }
                        else
                        {
                            continue;
                        }

                        if ((closeStart = internalUrl.IndexOf(")")) > 0)
                        {
                            closeEnd = closeStart + 1;
                        }
                        else if ((closeStart = internalUrl.IndexOf("%29", StringComparison.InvariantCulture)) > 0)
                        {
                            closeEnd = closeStart + 3;
                        }
                        else
                        {
                            continue;
                        }

                        // get value between ( ... )

                        String keyPart = internalUrl.Substring(openOffset, closeStart - openOffset);

                        bool resolved = false;

                        try
                        {
                            Object key = urlParser.ReadKey(keyPart);

                            if (key != null)
                            {
                                var dataUrl = DataUrlManager.GetUrl(registration.Type, key);
                                if (dataUrl != null)
                                {
                                    if (parsedHtml == null)
                                    {
                                        parsedHtml = new StringBuilder(html);
                                    }

                                    parsedHtml.Remove(match.Index, closeEnd);
                                    parsedHtml.Insert(match.Index, dataUrl);

                                    // check to see if this url started with a ~ and if so remove it

                                    if (match.Index > 0 && parsedHtml[match.Index - 1] == '~')
                                    {
                                        parsedHtml.Remove(match.Index - 1, 1);
                                    }

                                    resolved = true;
                                }
                            }
                        }
                        catch { }

                        if (!resolved)
                        {
                            _unhandledUrlAction(new UnhandledUrl { TypeName=registration.Name, Key=keyPart, Url=internalUrl });
                        }
                    }
                }
            }

            return parsedHtml != null ? parsedHtml.ToString() : html;
        }

        public static IData ParseUrl(String url)
        {
            if(url == null || url.Length == 0)
                return null;

            int startIndex = url.StartsWith("~") ? 1 : 0;

            foreach (var registration in _registrations.OrderByDescending(r => r.Name.Length))
            {
                // loop through all registrations and replace all internal urls with resolved external urls

                String typePrefix = String.Concat("/", registration.Name);

                var urlParser = registration.Parser ?? DefaultInternalUrlParser.Instance;

                // iterate matches in reverse order so we can do an inline replace and are not forces to call ToString

                if (url.IndexOf(typePrefix, startIndex) == startIndex)
                {
                    int openOffset, closeStart, closeEnd;

                    if (url[typePrefix.Length + startIndex] == '(')
                    {
                        openOffset = startIndex + typePrefix.Length + 1;
                    }
                    else if (url.IndexOf("%28", StringComparison.InvariantCulture) == typePrefix.Length + startIndex)
                    {
                        openOffset = startIndex + typePrefix.Length + 3;
                    }
                    else
                    {
                        continue;
                    }

                    if ((closeStart = url.IndexOf(")")) > 0)
                    {
                        closeEnd = closeStart + 1;
                    }
                    else if ((closeStart = url.IndexOf("%29", StringComparison.InvariantCulture)) > 0)
                    {
                        closeEnd = closeStart + 3;
                    }
                    else
                    {
                        continue;
                    }

                    // get value between ( ... )

                    String keyPart = url.Substring(openOffset, closeStart - openOffset);

                    try
                    {
                        Object key = urlParser.ReadKey(keyPart);

                        if (key != null)
                        {
                            var data = DataFacade.GetDataByUniqueKey(registration.Type, key);
                            if (data != null)
                            {
                                return data;
                            }
                        }

                    }
                    catch { }

                    // prefix matched but no data was found... return null as we will not find anything else where

                    _unhandledUrlAction(new UnhandledUrl { TypeName = registration.Name, Key = keyPart, Url = url });

                    return null;
                }
            }

            return null;
        }

        public static void SetUnhandledUrlAction(Action<UnhandledUrl> unhandledUrlAction)
        {
            if (unhandledUrlAction != null)
            {
                _unhandledUrlAction = unhandledUrlAction;
            }
        }

        internal class DataUrlRegistration
        {
            public String Name { get; set; }
            public Type Type { get; set; }
            public Delegate Function { get; set; }
            public IInternalUrlParser Parser { get; set; }
        }

        internal class TypeAssignabilityComparer : IComparer<DataUrlRegistration>
        {
            public static readonly TypeAssignabilityComparer Instance = new TypeAssignabilityComparer();

            public int Compare(DataUrlRegistration x, DataUrlRegistration y)
            {
                if (x.Type.IsAssignableFrom(y.Type))
                {
                    return 1;
                }
                else if (y.Type.IsAssignableFrom(x.Type))
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        private class DefaultInternalUrlParser : IInternalUrlParser
        {
            public static readonly DefaultInternalUrlParser Instance = new DefaultInternalUrlParser(); 

            public string WriteKey(IData Data)
            {
                return Data.GetUniqueKey().ToString();
            }

            public object ReadKey(String Key)
            {
                return Guid.Parse(Key);
            }
        }

        public class UnhandledUrl
        {
            public String TypeName { get; set; }
            public String Key { get; set; }
            public String Url { get; set; }
        }
    }
}