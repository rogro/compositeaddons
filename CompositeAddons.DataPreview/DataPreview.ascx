﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataPreview.ascx.cs" Inherits="CompositeAddons.Controls.DataPreview" %>

    <div style="display: none;">
        <asp:LinkButton runat="server" ID="RefreshButton" OnClick="RefreshButton_Click" Text="Refresh"></asp:LinkButton>
    </div>

    <iframe id="DataPreviewFrame" src="about:blank" runat="server"></iframe>

<script type="text/javascript">
    //<![CDATA[
    function refresh_frame() {

        var iframe = document.getElementById('<%= DataPreviewFrame.ClientID %>');
        iframe.src = iframe.src;

        if (iframe.src == "about:blank") {
            setTimeout(function () { __doPostBack('<%= RefreshButton.ClientID %>', '') }, 2500);
        }
    };
    //]]>
</script>
