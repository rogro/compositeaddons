﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Composite.C1Console.Forms;
using Composite.Plugins.Forms.WebChannel.UiControlFactories;
using System.Reflection;
using Composite.C1Console.Forms.WebChannel;
using CompositeAddons.DataUrl;

namespace CompositeAddons.Controls
{
    public partial class DataPreview : UserControlBasedUiControl
    {
        [FormsProperty()]
        public Object Id
        {
            get;
            set;
        }

        [FormsProperty()]
        public String DataType
        {
            get;
            set;
        }

        public override void BindStateToControlProperties()
        {
        }

        public override void InitializeViewState()
        {
            LoadPreview();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterOnSubmitStatement(typeof(Object), "refresh_frame", "refresh_frame();");
        }

        public void LoadPreview()
        {
            if (Id != null && DataType != null)
            {
                using (Composite.Data.DataScope scope = new Composite.Data.DataScope(Composite.Data.DataScopeIdentifier.Administrated))
                {
                    String dataUrl = DataUrlManager.GetUrl(DataType, Id);
                    if (dataUrl != null)
                    {
                        DataPreviewFrame.Src = dataUrl;
                    }
                }
            }
        }

        protected void RefreshButton_Click(object sender, EventArgs e)
        {
            LoadPreview();
        }
    }
}